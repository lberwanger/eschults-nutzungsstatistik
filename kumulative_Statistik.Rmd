---
title: "Nutzungsstatistik Oktober '23 bis April '24"
author: "Leonie Berwanger"
date: "generiert mit R, `r format(Sys.time(), '%d-%m-%Y')`"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
# Definiere Zeitraum
months <- c("Oktober23", "November23", "Dezember23", "Januar24", "Februar24", "März24", "April24")
# Für jeden der aufgelisteten Monate muss das Preprocessing durchgeführt und
# ein Monatsbericht erstellt worden sein
```


```{r}
# Einfache Berechnungen: Landing Page, Seitenaufrufe, Teilnahmebestätigungen, Tierarten

web_aufrufe <- c()
tet_aufrufe <- c()
landingpage_aufrufe <- c()

aufrufe_gesamt <- c()

teilnahme <- c()

schwein <- c()
rind <- c()

for (m in months){
  
  df <- read.csv(paste0("Nutzungsdaten/eSchulTS-Nutzungsdaten_", m, ".csv"),  sep=";", encoding="utf-8")
  
  web = df[df$URL == 1600646, "Aufrufe"]
  web_aufrufe <- c(web_aufrufe, web)
  tet = df[df$URL == "eschults2[1600646]", "Aufrufe"]
  tet_aufrufe <- c(tet_aufrufe, tet)
  landingpage_aufrufe <- c(landingpage_aufrufe, web+tet)
  
  ges = sum(df$Aufrufe)
  aufrufe_gesamt <- c(aufrufe_gesamt, ges)
  
  cert = sum(df[df$Seite == "Teilnahmebestätigung", "Aufrufe"])
  teilnahme <- c(teilnahme, cert)
  
  sw = sum(df[df$Tierart == "Schwein", "Aufrufe"])
  schwein <- c(schwein, sw)
  rd = sum(df[df$Tierart == "Rind", "Aufrufe"])
  rind <- c(rind, rd)
}

landingpage_total = data.frame(Monat=months, WebAufrufe=web_aufrufe, tetAufrufe=tet_aufrufe, GesamtAufrufe=landingpage_aufrufe)
# landingpage_total

aufrufe_total = data.frame(Monat=months, Seitenaufrufe=aufrufe_gesamt)
# aufrufe_total

teilnahme_total = data.frame(Monat=months, Teilnahmebestätigungen=teilnahme)
# teilnahme_total

tierarten_total = data.frame(Monat=months, Schwein=schwein, Rind=rind)
# tierarten_total

```
# Zugriffe auf die Landing Page
```{r}

ms_plt <-c()
accessmod_plt <- c()
hits_plt <- c()
for(i in rownames(landingpage_total)){
  row <- landingpage_total[i,]
  ms_plt <- c(ms_plt, row$Monat)
  ms_plt <- c(ms_plt, row$Monat)
  accessmod_plt <- c(accessmod_plt, "web")
  hits_plt <- c(hits_plt, row$WebAufrufe)
  accessmod_plt <- c(accessmod_plt, "tet")
  hits_plt <- c(hits_plt, row$tetAufrufe)
}

landingpage_plot <- data.frame(Link=accessmod_plt, Monat=ms_plt, Aufrufe=hits_plt)
# landingpage_plot

landingpage_plot$Monat <- factor(landingpage_plot$Monat, levels = unique(landingpage_plot$Monat))

library(ggplot2)
ggplot(landingpage_plot, aes(x=Monat, y=Aufrufe, fill=Link)) + geom_col()

knitr::kable(landingpage_total, col.names = c("Monat", "WebAufrufe", "tetAufrufe", "GesamtAufrufe"), row.names = F)

```

# Seitenaufrufe gesamt
```{r}

aufrufe_total$Monat <- factor(aufrufe_total$Monat, levels = unique(aufrufe_total$Monat))

library(ggplot2)
ggplot(aufrufe_total, aes(Monat, Seitenaufrufe, group=1)) + geom_line() + geom_point()

knitr::kable(aufrufe_total, col.names = c("Monat", "Seitenaufrufe"), row.names = F)
```

# Abgerufene Teilnahmebestätigungen
```{r}
teilnahme_total$Monat <- factor(teilnahme_total$Monat, levels = unique(teilnahme_total$Monat))

library(ggplot2)
ggplot(teilnahme_total, aes(x=Monat, y=Teilnahmebestätigungen)) + geom_col()

knitr::kable(teilnahme_total, col.names = c("Monat", "Teilnahmebestätigungen"), row.names = F)
```
\newpage
# Aufrufe nach Büchern (kumulativ)
```{r}

for(m in months){
  
}
df <- read.csv(paste0("Nutzungsdaten/eSchulTS-Nutzungsdaten_", m, ".csv"),  sep=";", encoding="utf-8")

books_df <- read.csv(paste0("BuchSeiten/BuchAufrufe", months[1], ".csv"),  sep=";", encoding="utf-8")

for(i in 2:length(months)){
    books_i <- read.csv(paste0("BuchSeiten/BuchAufrufe", months[i], ".csv"),  sep=";", encoding="utf-8")
    for(b in books_df$buecher){
      books_df$aufrufe[books_df$buecher==b] <- books_df$aufrufe[books_df$buecher==b] + books_i$aufrufe[books_i$buecher==b]
    }
}

books_df <- books_df[order(-books_df$aufrufe),]
# books_df

knitr::kable(books_df, col.names = c("Buch", "Aufrufe"), row.names = F)
```

\newpage
# Aufrufe nach Tierart (kumulativ)
```{r}

species <- read.csv("Codierung/BuchTierart.csv", sep=";", encoding = "utf-8")
book_species_total <- merge(books_df, species, by.x="buecher", by.y="Buch")
pigs <- sum(book_species_total$aufrufe[book_species_total$Tierart=="Schwein"])
cattle <- sum(book_species_total$aufrufe[book_species_total$Tierart=="Rind"])

library(ggplot2)
plotdata <- data.frame(Tierart=c("Schwein","Rind"), Aufrufe=c(pigs,cattle))
pct <- prop.table(c(pigs,cattle))
plot <- ggplot(data=plotdata, aes(x=Tierart,y=Aufrufe,fill=Tierart)) + geom_bar(stat="identity") +
geom_text(aes(label=scales::percent(pct))) +
theme_minimal()
plot+scale_fill_brewer(palette=3)

knitr::kable(tierarten_total, col.names = c("Monat", "Schwein", "Rind"), row.names = F)

```

\newpage
## Top 5 Seiten (außer Landing Page etc.) im Zeitraum 

Im Zeitraum `r months[1]` bis `r months[length(months)]` wurden die folgenden 5 Seiten am häufigsten aufgerufen:

```{r}

seiten_total <- read.csv(paste0("Nutzungsdaten/eSchulTS-Nutzungsdaten_", months[1], ".csv"),  sep=";", encoding="utf-8")
seiten_total <- seiten_total[!seiten_total$Buch == "eSchulTS²", c("URL", "Seite", "Buch", "Aufrufe")]

for(m in months[-1]){
  df <- read.csv(paste0("Nutzungsdaten/eSchulTS-Nutzungsdaten_", m, ".csv"),  sep=";", encoding="utf-8")
  seiten <- df[!df$Buch=="eSchulTS²", c("URL", "Seite", "Buch", "Aufrufe")]
  seiten_total <- merge(seiten_total, seiten, by=c("URL","Buch", "Seite"), all.x =TRUE, all.y=TRUE)
  seiten_total <- transform(seiten_total, GESAMT = rowSums(seiten_total[,4:ncol(seiten_total)], na.rm=TRUE))
  seiten_total <- seiten_total[,c("URL","Seite","Buch","GESAMT")]
}

top5_total <- seiten_total[order(-seiten_total$GESAMT),]
top5_total <- top5_total[1:5,c("Seite", "Buch", "GESAMT")]
rownames(top5_total) <- c(1:5)

knitr::kable(top5_total, col.names = c("Seite","Buch","Aufrufe"), row.names = T)

```


