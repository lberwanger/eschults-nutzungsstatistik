# Nutzungsstatistik für eSchulTS - E-Learning-Schulungsunterlagen zur Verbesserung des Tierschutzes bei Transport und Schlachtung von Rind und Schwein 

## Projektbeschreibung
Monatliche Nutzungsstatistiken für die eSchulTS-Plattform, automatisiert erstellt in R.
Erhobene Metriken:
- Zugriffe auf die Landing Page (externe vs. interne Nutzer:innen)
- Ausgewählte Sprachen
- Seitenaufrufe gesamt (externe vs. interne Nutzer:innen)
- Seitenaufrufe nach Tierart
- Seitenaufrufe nach Büchern
- Top 5 Seiten
- Aufgerufene Teilnahmebestätigungen
Die Seitenaufrufe gesamt werden zusätzlich kumulativ berechnet.

## Beispiele (TODO)

## Installation (TODO)
- [ ] RStudio installieren
- [ ] Projekt aus Versions-Kontrolle importieren
- [ ] Nutzungsdaten von E:

## Nutzung (TODO)

## Hilfe
Für weitere Informationen und Technischen Support kontaktieren Sie: [leonie.berwanger2@fu-berlin.de](mailto:leonie.berwanger2@fu-berlin.de).

## Roadmap (TODO)


## Project status
active
